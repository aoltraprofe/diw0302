#README / LEEME

*Práctica:* 11

*Unidad:* UD03. Responsive Design

*Módulo:* Diseño de Interfaces WEB

*Ciclo Formativo:* Desarrollo de Aplicaciones WEB

*Objetivo:* Menú adaptativo con desplazamiento lateral

##Author / Autor

Alfredo Oltra (mail: [alfredo.ptcf@gmail.com](alfredo.ptcf@gmail.com) / twitter: [@aoltra](https://twitter.com/aoltra))

##Licencias

Iconos diseñados por [Freepik](http://www.flaticon.es/autores/freepik) desde [flaticon](http://www.flaticon.com) con licencia [CC 3.0](http://creativecommons.org/licenses/by/3.0/)

